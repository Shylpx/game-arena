require 'csv'

letters = CSV.read('./list.csv', headers: true).map do |category|
  CSV.read("./lists/#{category['file']}.csv").to_a
rescue Errno::ENOENT => _
end.compact.flatten.map { |w| w.split('') }.flatten.uniq.sort.join

puts(letters)
