require 'google/cloud/firestore'
require 'csv'

# ENV['FIRESTORE_EMULATOR_HOST'] = '127.0.0.1:8080'

firestore = Google::Cloud::Firestore.new project_id: 'hangman-57a37'

list = CSV.read('./list.csv', headers: true)
collection = firestore.col 'term_categories'
list.each do |category|
  content = CSV.read("./lists/#{category['file']}.csv").flatten
  puts " > [#{category['name']}] - #{category['language']}"

  document = collection.add(
    category: category['name'],
    language: category['language'],
    list: content.flatten
  )
rescue Errno::ENOENT => _
  puts 'Error: File not found'
end

