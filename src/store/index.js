import Vue from "vue";
import Vuex from "vuex";
import * as firebase from "firebase";

Vue.use(Vuex);

const HIDDEN_CHAR = "_";

export default new Vuex.Store({
  state: {
    user: null,
    room: null,
    game: null,
    playerCard: null,
    status: "loading"
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },
    removeUser(state) {
      state.user = null;
    },
    setRoom(state, payload) {
      state.room = payload;
    },
    setGame(state, payload) {
      state.game = payload;
    },
    setStatus(state, payload) {
      state.status = payload;
    },
    updateTurnInfo(state, payload) {
      state.game.last_turn_started_at = payload.last_turn_started_at;
      state.game.current_player = payload.current_player;
    }
  },
  actions: {
    // NOTA: Data loading

    fetchUser({ commit, dispatch }, payload) {
      if (!payload) {
        commit("setUser", null);
        commit("setStatus", "success");
        return;
      }

      if (payload.guest) {
        commit("setUser", payload);
        dispatch("loadRoom");
        return;
      }

      firebase
        .firestore()
        .collection("users")
        .doc(payload.uid)
        .get()
        .then(user => {
          commit("setUser", {
            uid: payload.uid,
            name: user.data().name
          });

          dispatch("loadRoom");
        });
    },
    loadRoom({ commit, getters, dispatch }) {
      firebase
        .firestore()
        .collection("rooms")
        .where("state", "in", ["open", "ready"])
        .where("member_uids", "array-contains", getters.user.uid)
        .get()
        .then(querySnapshot => {
          const room = querySnapshot.docs[0];
          commit("setRoom", querySnapshot.empty ? null : { uid: room.id, ...room.data() });

          if (this.room) commit("setStatus", "success");
          else dispatch("loadGame");
        });
    },
    loadGame({ commit, getters }) {
      firebase
        .firestore()
        .collection("games")
        .where("state", "==", "playing")
        .where(`player_cards.${getters.user.uid}.beacon`, "==", true)
        .get()
        .then(querySnapshot => {
          if (querySnapshot.empty) commit("setGame", null);
          else {
            const game = querySnapshot.docs[0];
            commit("setGame", { uid: game.id, ...game.data() });
          }
          commit("setStatus", "success");
        })
        .catch(() => {
          commit("setStatus", "failure");
        });
    },

    // NOTE: User actions

    signUp({ commit }, payload) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
          firebase
            .firestore()
            .collection("users")
            .doc(response.user.uid)
            .set({ name: payload.nickname })
            .then(() => {
              commit("setStatus", "success");
            });
        })
        .catch(() => {
          commit("setStatus", "failure");
        });
    },
    signIn({ commit }, payload) {
      firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password)
        .then(() => {
          commit("setStatus", "success");
        })
        .catch(() => {
          commit("setStatus", "failure");
        });
    },
    signOut({ commit }) {
      firebase
        .auth()
        .signOut()
        .then(() => {
          commit("setStatus", "success");
        })
        .catch(() => {
          commit("setStatus", "failure");
        });
    },

    // NOTE: Game actions

    sendKey({ getters }, payload) {
      firebase.functions().httpsCallable("board-selectKey")({
        key: payload.key,
        gameUid: getters.game.uid,
        ...(getters.user.guest && { guest: getters.user })
      });
    }
  },
  getters: {
    // NOTE: Constants

    hiddenChar() {
      return HIDDEN_CHAR;
    },

    // NOTE: Base state data

    status(state) {
      return state.status;
    },
    user(state) {
      return state.user;
    },
    loggedIn(state) {
      return state.user !== null;
    },
    room(state) {
      return state.room;
    },
    game(state) {
      return state.game;
    },

    // NOTE: Game current player

    currentPlayerUid(state) {
      if (!state.game) return;

      return state.game.current_player;
    },
    currentPlayer(state, getters) {
      if (!state.game) return;

      return state.game.player_cards[getters.currentPlayerUid];
    },
    isUserTurn(state, getters) {
      if (!state.game) return;

      return getters.currentPlayerUid == getters.user.uid;
    },

    // NOTE: Game config

    maxFailures(state) {
      if (!state.game) return;

      return state.game.max_failures;
    },
    string(state) {
      if (!state.game) return;

      return state.game.string;
    },

    // NOTE: Game turns

    playingState(state) {
      if (!state.game) return false;

      return state.game.state === "playing";
    },
    turnStartedAt(state) {
      if (!state.game) return 0;

      return state.game.last_turn_started_at.seconds;
    },
    turnSeconds(state) {
      if (!state.game) return;

      return state.game.turn_seconds;
    },

    // NOTE: Player card properties

    playerCards(state) {
      if (!state.game) return;

      return state.game.player_cards;
    },

    playerFailures(state) {
      if (!state.game) return () => null;

      return userUid => state.game.player_cards[userUid].failures;
    },

    playerString(state) {
      if (!state.game) return () => null;

      return userUid => state.game.player_cards[userUid].string;
    },

    playerSelectedKeys(state) {
      if (!state.game) return () => null;

      return userUid => state.game.player_cards[userUid].selected_keys;
    },

    playerLastSelectedKeyAt(state) {
      if (!state.game) return () => null;

      return userUid => state.game.player_cards[userUid].last_selected_key_at;
    },

    // NOTE: Player states

    playerWon(state, getters) {
      if (!state.game) return () => null;

      return userUid => !state.game.player_cards[userUid].string.includes(getters.hiddenChar);
    },

    playerLost(state, getters) {
      if (!state.game) return () => null;

      return userUid => getters.playerFailures(userUid) >= getters.maxFailures;
    },

    // NOTE: Utils

    now: () => () => {
      return Math.floor(Date.now() / 1000);
    }
  },
  modules: {}
});
