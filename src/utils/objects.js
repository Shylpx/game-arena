export function sortObjectBy(object, property) {
  return Object.fromEntries(
    Object.entries(object).sort((element1, element2) => {
      return element1[1][property] - element2[1][property];
    })
  );
}
