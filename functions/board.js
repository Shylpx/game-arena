const functions = require("firebase-functions");
const admin = require("firebase-admin");
const { FieldValue } = require("firebase-admin/firestore");
const turns = require("./turns");

const HIDDEN_CHAR = "_";
const VISIBLE_CHAR = "x";

exports.selectKey = functions.https.onCall(async (data, context) => {
  const gameUid = data.gameUid;
  const userUid = (context.auth || data.guest).uid;

  const key = data.key;

  if (key.length === 0)
    throw new functions.https.HttpsError("invalid-argument", "Empty 'key' argument.");

  const game = (
    await admin
      .firestore()
      .collection("games")
      .doc(gameUid)
      .get()
  ).data();

  if (game.current_player !== userUid || game.state !== "playing") return;

  const playerCards = game.player_cards;
  const string = game.string;
  let playerString = playerCards[userUid].string;
  let success = false;

  // NOTE: Letter
  if (key.length === 1) {
    let indexes = allIndexes(string, key);
    success = indexes.length > 0;

    playerString = indexes.reduce((string, index) => {
      return string.substr(0, index) + VISIBLE_CHAR + string.substr(index + 1);
    }, playerString);
  }

  // NOTE: Sentences
  else {
    success = sanitize(key) === sanitize(string);

    if (success) {
      playerString = Array(string.length).join(VISIBLE_CHAR);
    }
  }

  // NOTE: Check win condition
  const currentFailures = playerCards[userUid].failures + (success ? 0 : 1);
  const playerCount = Object.keys(playerCards).length;
  const stringCompleted = !playerString.includes(HIDDEN_CHAR);
  const stillAlive = currentFailures < game.max_failures;
  const playing =
    (!stringCompleted && stillAlive) ||
    Object.values(playerCards).filter(player => !player.position).length > 2;
  const playerPositions = Object.values(playerCards).map(player => player.position);
  const newPosition = calculatePosition(playerPositions, playerCount, stringCompleted, !stillAlive);
  const lastPlayerUid = playing
    ? null
    : Object.keys(playerCards).find(uid => {
        return userUid !== uid && !playerCards[uid].position;
      });

  await admin
    .firestore()
    .collection("games")
    .doc(gameUid)
    .set(
      {
        state: playing ? "playing" : "completed",
        player_cards: {
          [userUid]: {
            selected_keys: FieldValue.arrayUnion(key),
            string: playerString,
            failures: currentFailures,
            position: newPosition
          },
          ...(lastPlayerUid
            ? {
                [lastPlayerUid]: {
                  position: calculatePosition(
                    playerPositions + [newPosition],
                    playerCount,
                    false,
                    true
                  )
                }
              }
            : {})
        }
      },
      { merge: true }
    );

  if (!playing) return;

  nextTurn(gameUid, game.turn_seconds, userUid, playerCards);
});

function allIndexes(word, letter) {
  positions = [];

  for (var i = 0; i < word.length; ) {
    var position = sanitize(word).indexOf(sanitize(letter), i);

    if (position === -1) break;

    positions.push(position);
    i = position + 1;
  }

  return positions;
}

// NOTE: This method should match the `sanitize` method on `functions`
function sanitize(string) {
  return string
    .normalize("NFD")
    .replace(/(?!\u0303)\p{Diacritic}/gu, "")
    .normalize("NFC")
    .toLowerCase();
}

function calculatePosition(playerPositions, playerCount, won, lost) {
  if (!won && !lost) return null;

  let position = won ? 1 : playerCount;
  while (playerPositions.includes(position)) {
    position += won ? 1 : -1;
  }
  return position;
}

function nextTurn(gameUid, delaySeconds, currentPlayer, playerCards) {
  turns.assignNextPlayer(
    gameUid,
    delaySeconds,
    turns.calculateNextPlayer(currentPlayer, playerCards),
    false
  );
}
