const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp();

// Modules
exports.rooms = require("./rooms");
exports.board = require("./board");
exports.turns = require("./turns");
